#include "tests.h"

void tests :: test_EndGeometricRow()
{
    int x = 0;
    double E = 0.000001;
    double m = 2.0;
    double check;
    check = EndGeometricRow(x,E,m);
    QCOMPARE(1.0,check);
    x = 2.0;
    check = EndGeometricRow(x,E,m);
    QCOMPARE(3.0,check);
    x = 4.0;
    check = EndGeometricRow(x,E,m);
    QCOMPARE(5,check);
}
