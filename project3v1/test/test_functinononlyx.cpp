#include "tests.h"

void tests :: test_functionOnlyX()
{
    double x = 0.2;
    double E = 0.000001;
    double check;
    QString check1 = QString :: number(functionOnlyX(x,E));
    QCOMPARE("1.25",check1);
    x = 0;
    check = functionOnlyX(x,E);
    QCOMPARE(1,check);
    E = 2;
    check = functionOnlyX(x,E);
    QCOMPARE(1,check);
}
