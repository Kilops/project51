#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->groupBox->setVisible(false);
    ui->groupBox_2->setVisible(false);
    ui->textEdit->setEnabled(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
*\brief Виджет функции Edegree
*/
void MainWindow::on_pushButton_eX_clicked()
{;
    double x = ui->doubleSpinBox_12->value();
    double e = ui->doubleSpinBox_13->value();
    double buffer = Edegree(x,e);
    QString str = QString :: number(buffer, 'g', 7);
    ui->lineEdit->setText(str);
}

void MainWindow::on_pushButton_sinx_clicked()
{
    double x = ui->doubleSpinBox_4->value();
    double E = ui->doubleSpinBox_5->value();
    double e = sinx(x,E);
    QString str = QString :: number(e, 'g', 7);
    ui->lineEdit_2->setText(str);
}

void MainWindow::on_pushButton_cosx_clicked()
{
    double x = ui->doubleSpinBox_10->value();
    double E = ui->doubleSpinBox_11->value();
    double e = cosx(x,E);
    QString str = QString :: number(e, 'g', 7);
    ui->lineEdit_6->setText(str);
}

void MainWindow::on_pushButton_lnx_clicked()
{
    double x = ui->doubleSpinBox_8->value();
    double E = ui->doubleSpinBox_9->value();
    if((x > 1.0) || (x <= -1.0))
    {
        QMessageBox :: warning(this, "ОШИБКА", "x не принадлежит промежутку  (-1, 1]");
    }else {
        double e = lnx(x,E);
        QString str = QString :: number(e, 'g', 7);
        ui->lineEdit_3->setText(str);
    }
}

void MainWindow::on_pushButton_function_clicked()
{
    double x = ui->doubleSpinBox_6->value();
    double E = ui->doubleSpinBox_7->value();
    double a = ui->doubleSpinBox_14->value();
    if((x >= 1.0) || (x <= -1.0))
    {
        QMessageBox :: warning(this, "ОШИБКА", "x не принадлежит промежутку  (-1, 1)");
    }else {
        double e = function(x,E,a);
        QString str = QString :: number(e, 'g', 7);
        ui->lineEdit_4->setText(str);
    }
}

void MainWindow::on_pushButton_arctg_clicked()
{
    double x = ui->doubleSpinBox_2->value();
    double E = ui->doubleSpinBox_3->value();
    if((x > 1.0) || (x < -1.0))
    {
        QMessageBox :: warning(this, "ОШИБКА", "x не принадлежит промежутку  [-1, 1]");
    }else {
        double e = arctgx(x,E);
        QString str = QString :: number(e, 'g', 7);
        ui->lineEdit_5->setText(str);
    }
}

void MainWindow::on_pushButton_close_clicked()
{
    close();
}

void MainWindow::on_pushButton_8_clicked()
{
    ui->textEdit->setVisible(false);
    ui->groupBox->setVisible(true);
    ui->groupBox_2->setVisible(true);
    ui->pushButton_8->setVisible(false);
}

void MainWindow::on_pushButton_EndGeometricRow_clicked()
{
    double x = ui->doubleSpinBox_17->value();
    double E = ui->doubleSpinBox_18->value();
    double m = ui->doubleSpinBox_19->value();
    if((x == 1.0) || (m <= 0.0))
    {
        QMessageBox :: warning(this, "ОШИБКА", "X равно 1 или M не принадлежит натуральным числам");
    }else {
        double e = EndGeometricRow(x,E,m);
        QString str = QString :: number(e, 'g', 7);
        ui->lineEdit_7->setText(str);
    }
}

void MainWindow::on_pushButton_functionOnlyX_clicked()
{
    double x = ui->doubleSpinBox_15->value();
    double E = ui->doubleSpinBox_16->value();
    if((x > 1.0) || (x < -1.0))
    {
        QMessageBox :: warning(this, "ОШИБКА", "x не принадлежит промежутку  [-1, 1]");
    }else {
        double e = functionOnlyX(x,E);
        QString str = QString :: number(e, 'g', 7);
        ui->lineEdit_8->setText(str);
    }
}
