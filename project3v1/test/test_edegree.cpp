#include "tests.h"

void tests :: test_Edegrees()
{
    int x = 0;
    double E = 1.0;
    double check;
    check = Edegree(x,E);
    QCOMPARE(1.0,check);
    x = 1.0;
    check = Edegree(x,E);
    QCOMPARE(2.0,check);
    x = 2.0;
    E = 2.0;
    check = Edegree(x,E);
    QCOMPARE(0.0,check);
}
