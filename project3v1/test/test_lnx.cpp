#include "tests.h"

void tests :: test_lnx()
{
    double x = 0;
    double E = 0.000001;
    double check;
    check = lnx(x,E);
    QCOMPARE(0,check);
    x = 0.2;
    check = lnx(x,E);
    QString str = QString :: number(check);
    QString str1 = QString :: number(0.1823218);
    QCOMPARE (str1,str);
    x = 0.1;
    E = 1.0;
    check = lnx(x,E);
    str = QString :: number(check);
    str1 = QString :: number(0);
    QCOMPARE (str1,str);
}
