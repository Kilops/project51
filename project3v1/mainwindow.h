/**
*\file
*\brief
Модуль программы по работе с оконными приложением
*\author Авдеев
*\version 1.0
*
* Виджеты для работы оконного приложения
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QMessageBox>
#include "function.h"



namespace Ui {
class MainWindow;
}
//! Класс MainWindow
/*!
Класс для работы с оконными приложением
*/
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //! Функция для создания обьекты класса Mainwindow
    /*!
     * \brief MainWindow
     * \param parent
     */
    explicit MainWindow(QWidget *parent = nullptr);
    //! Функция для уничтожения класса
    /*!
      *\brief ~MainWindow
      */
    ~MainWindow();

public slots:
    /*!
    Виджет функции Edegree
    */
    void on_pushButton_eX_clicked();

    /*!
    *Виджет функции function
    */
    void on_pushButton_function_clicked();

    /*!
    *Виджет функции lnx
    */
    void on_pushButton_lnx_clicked();

    /*!
    *Виджет функции sinx
    */
    void on_pushButton_sinx_clicked();

    /*!
    *Виджет функции cosx
    */
    void on_pushButton_cosx_clicked();

    /*!
    *Виджет функции arctg
    */
    void on_pushButton_arctg_clicked();

    /*!
    *Виджет для закрытия программы
    */
    void on_pushButton_close_clicked();

    /*!
    *Виджет начала работы
    */
    void on_pushButton_8_clicked();

    /*!
    *Виджет функции EndGeometricRow
    */
    void on_pushButton_EndGeometricRow_clicked();

    /*!
    *Виджет функции functionOnlyX
    */
    void on_pushButton_functionOnlyX_clicked();

private:
    /*!
     * \brief ui
     * обьект класса MainWindow
     */
    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H
