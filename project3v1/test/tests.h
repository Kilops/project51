#ifndef TESTS_H
#define TESTS_H

#include "../function.h"
#include <QTest>
#include <QtCore>

class tests : public QObject
{
Q_OBJECT

public:
    tests();

private slots:
    void test_Edegrees();
    void test_function();
    void test_sinx ();
    void test_cosx();
    void test_lnx();
    void test_arctgx();
    void test_EndGeometricRow();
    void test_functionOnlyX();
};

#endif // TESTS_H
