/**
*\file
*\brief
Модуль программы по нахождению значения некоторых функций с помощью рядов Маклорена
*\author Авдеев
*\version 1.0
*
* Функции для нахождения значений функций с помощью рядов Маклорена
*/

#ifndef FUNCTION_H
#define FUNCTION_H

#include <QDebug>

/**
*\brief функция функция нахождения \f$ e^{x} \f$
*\param X значения функции
*\param E точность
*\return возвращает значение функции \f$ e^{x} \f$
*/
double Edegree (double x, double E);

/**
*\brief функция функция нахождения \f$ \sin (x) \f$
*\param X значения функции
*\param E точность
*\return возвращает значение функции \f$ \sin (x) \f$
*/
double sinx (double x, double E);

/**
*\brief функция функция нахождения \f$ \cos (x) \f$
*\param X значения функции
*\param E точность
*\return возвращает возвращает значение функции \f$ \cos (x) \f$
*/
double cosx (double x, double E);

/**
*\brief функция функция нахождения \f$ \ln (1+x) \f$
*\param X значения функции
*\param E точность
*\return возвращает значение функции \f$ \ln (1+x) \f$
*/
double lnx (double x, double E);

/**
*\brief функция функция нахождения \f$ arctg (x) \f$
*\param X значения функции
*\param E точность
*\return возвращает значение функции \f$ arctg (x) \f$
*/
double arctgx (double x, double E);

/**
*\brief функция проверки на отрицательное значение
*\param сheks проверяемое число
*\return возвращает положительное число
*/
double check (double cheks);

/**
*\brief функция нахождения \f$ (1+x)^{a} \f$
*\param X значения функции
*\param E точность
*\return возвращает значение функции \f$ (1+x)^{a} \f$
*/
double function (double x, double E, double a);

/**
*\brief функция нахождения \f$ \frac {1} {1-x} \f$
*\param X значения функции
*\param E точность
*\return возвращает значение функции \f$ \frac {1} {1-x} \f$
*/
double functionOnlyX (double x, double E);

/**
*\brief функция нахождения функция нахождения \f$ \frac {1-x^{m+1}} {1-x} \f$
*\param X значения функции
*\param E точность
*\return возвращает значение функции \f$ \frac {1-x^{m+1}} {1-x} \f$
*/
double EndGeometricRow (double x, double E, double m);


#endif // FUNCTION_H
