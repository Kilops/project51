#include "tests.h"

void tests :: test_cosx ()
{
    double x = 0;
    double E = 2;
    double check;
    check = cosx(x,E);
    QCOMPARE (0,check);
    x=1;
    E = 1;
    check = cosx(x,E);
    QCOMPARE (1,check);
    x=10;
    check = cosx(x,E);
    QString str = QString :: number(check);
    QString str1 = QString :: number(-0.620489);
    QCOMPARE (str1,str);
}
