#include "tests.h"

void tests :: test_function()
{
    double x = 0;
    double E = 0.000001;
    double a = 2;
    double check;
    check = function(x,E,a);
    QCOMPARE(1.0,check);
    x = 0.2;
    check = function(x,E,a);
    QCOMPARE(1.44,check);
    x = 0.1;
    E = 1.0;
    check = function(x,E,a);
    QCOMPARE(1,check);
}
