/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton_close;
    QPushButton *pushButton_8;
    QTextEdit *textEdit;
    QGroupBox *groupBox_2;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_36;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_33;
    QPushButton *pushButton_arctg;
    QPushButton *pushButton_function;
    QPushButton *pushButton_lnx;
    QPushButton *pushButton_cosx;
    QPushButton *pushButton_sinx;
    QPushButton *pushButton_eX;
    QPushButton *pushButton_functionOnlyX;
    QPushButton *pushButton_EndGeometricRow;
    QGridLayout *gridLayout_29;
    QGridLayout *gridLayout_30;
    QLabel *label_22;
    QDoubleSpinBox *doubleSpinBox_17;
    QGridLayout *gridLayout_31;
    QLabel *label_23;
    QDoubleSpinBox *doubleSpinBox_18;
    QGridLayout *gridLayout_32;
    QLabel *label_24;
    QDoubleSpinBox *doubleSpinBox_19;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QDoubleSpinBox *doubleSpinBox_2;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QDoubleSpinBox *doubleSpinBox_3;
    QGridLayout *gridLayout_7;
    QGridLayout *gridLayout_8;
    QLabel *label_5;
    QDoubleSpinBox *doubleSpinBox_6;
    QGridLayout *gridLayout_9;
    QLabel *label_6;
    QDoubleSpinBox *doubleSpinBox_7;
    QGridLayout *gridLayout_19;
    QLabel *label_13;
    QDoubleSpinBox *doubleSpinBox_14;
    QGridLayout *gridLayout_10;
    QGridLayout *gridLayout_11;
    QLabel *label_7;
    QDoubleSpinBox *doubleSpinBox_8;
    QGridLayout *gridLayout_12;
    QLabel *label_8;
    QDoubleSpinBox *doubleSpinBox_9;
    QGridLayout *gridLayout_13;
    QGridLayout *gridLayout_14;
    QLabel *label_9;
    QDoubleSpinBox *doubleSpinBox_10;
    QGridLayout *gridLayout_15;
    QLabel *label_10;
    QDoubleSpinBox *doubleSpinBox_11;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_5;
    QLabel *label_3;
    QDoubleSpinBox *doubleSpinBox_4;
    QGridLayout *gridLayout_6;
    QLabel *label_4;
    QDoubleSpinBox *doubleSpinBox_5;
    QGridLayout *gridLayout_16;
    QGridLayout *gridLayout_17;
    QLabel *label_11;
    QDoubleSpinBox *doubleSpinBox_12;
    QGridLayout *gridLayout_18;
    QLabel *label_12;
    QDoubleSpinBox *doubleSpinBox_13;
    QGridLayout *gridLayout_20;
    QGridLayout *gridLayout_27;
    QLabel *label_20;
    QDoubleSpinBox *doubleSpinBox_15;
    QGridLayout *gridLayout_28;
    QLabel *label_21;
    QDoubleSpinBox *doubleSpinBox_16;
    QGridLayout *gridLayout_21;
    QLabel *label_19;
    QLineEdit *lineEdit;
    QGridLayout *gridLayout_22;
    QLabel *label_15;
    QLineEdit *lineEdit_2;
    QGridLayout *gridLayout_23;
    QLineEdit *lineEdit_6;
    QLabel *label_16;
    QGridLayout *gridLayout_24;
    QLineEdit *lineEdit_3;
    QLabel *label_17;
    QGridLayout *gridLayout_25;
    QLabel *label_14;
    QLineEdit *lineEdit_4;
    QGridLayout *gridLayout_26;
    QLabel *label_18;
    QLineEdit *lineEdit_5;
    QGridLayout *gridLayout_34;
    QLabel *label_25;
    QLineEdit *lineEdit_7;
    QGridLayout *gridLayout_35;
    QLabel *label_26;
    QLineEdit *lineEdit_8;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1689, 937);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        pushButton_close = new QPushButton(centralWidget);
        pushButton_close->setObjectName(QString::fromUtf8("pushButton_close"));
        pushButton_close->setGeometry(QRect(120, 220, 93, 28));
        pushButton_8 = new QPushButton(centralWidget);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setGeometry(QRect(20, 220, 95, 28));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(20, 0, 651, 211));
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(20, 270, 1711, 521));
        layoutWidget = new QWidget(groupBox_2);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(1, 20, 1532, 440));
        gridLayout_36 = new QGridLayout(layoutWidget);
        gridLayout_36->setSpacing(6);
        gridLayout_36->setContentsMargins(11, 11, 11, 11);
        gridLayout_36->setObjectName(QString::fromUtf8("gridLayout_36"));
        gridLayout_36->setContentsMargins(0, 0, 0, 0);
        groupBox = new QGroupBox(layoutWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_33 = new QGridLayout(groupBox);
        gridLayout_33->setSpacing(6);
        gridLayout_33->setContentsMargins(11, 11, 11, 11);
        gridLayout_33->setObjectName(QString::fromUtf8("gridLayout_33"));
        pushButton_arctg = new QPushButton(groupBox);
        pushButton_arctg->setObjectName(QString::fromUtf8("pushButton_arctg"));

        gridLayout_33->addWidget(pushButton_arctg, 0, 0, 1, 1);

        pushButton_function = new QPushButton(groupBox);
        pushButton_function->setObjectName(QString::fromUtf8("pushButton_function"));

        gridLayout_33->addWidget(pushButton_function, 0, 1, 1, 1);

        pushButton_lnx = new QPushButton(groupBox);
        pushButton_lnx->setObjectName(QString::fromUtf8("pushButton_lnx"));

        gridLayout_33->addWidget(pushButton_lnx, 0, 2, 1, 1);

        pushButton_cosx = new QPushButton(groupBox);
        pushButton_cosx->setObjectName(QString::fromUtf8("pushButton_cosx"));

        gridLayout_33->addWidget(pushButton_cosx, 0, 3, 2, 1);

        pushButton_sinx = new QPushButton(groupBox);
        pushButton_sinx->setObjectName(QString::fromUtf8("pushButton_sinx"));

        gridLayout_33->addWidget(pushButton_sinx, 0, 4, 2, 1);

        pushButton_eX = new QPushButton(groupBox);
        pushButton_eX->setObjectName(QString::fromUtf8("pushButton_eX"));

        gridLayout_33->addWidget(pushButton_eX, 0, 5, 1, 1);

        pushButton_functionOnlyX = new QPushButton(groupBox);
        pushButton_functionOnlyX->setObjectName(QString::fromUtf8("pushButton_functionOnlyX"));

        gridLayout_33->addWidget(pushButton_functionOnlyX, 0, 6, 2, 1);

        pushButton_EndGeometricRow = new QPushButton(groupBox);
        pushButton_EndGeometricRow->setObjectName(QString::fromUtf8("pushButton_EndGeometricRow"));

        gridLayout_33->addWidget(pushButton_EndGeometricRow, 0, 7, 1, 1);

        gridLayout_29 = new QGridLayout();
        gridLayout_29->setSpacing(6);
        gridLayout_29->setObjectName(QString::fromUtf8("gridLayout_29"));
        gridLayout_30 = new QGridLayout();
        gridLayout_30->setSpacing(6);
        gridLayout_30->setObjectName(QString::fromUtf8("gridLayout_30"));
        label_22 = new QLabel(groupBox);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        gridLayout_30->addWidget(label_22, 0, 0, 1, 1);

        doubleSpinBox_17 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_17->setObjectName(QString::fromUtf8("doubleSpinBox_17"));
        doubleSpinBox_17->setDecimals(6);
        doubleSpinBox_17->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_17->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_17->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_30->addWidget(doubleSpinBox_17, 0, 1, 1, 1);


        gridLayout_29->addLayout(gridLayout_30, 0, 0, 1, 1);

        gridLayout_31 = new QGridLayout();
        gridLayout_31->setSpacing(6);
        gridLayout_31->setObjectName(QString::fromUtf8("gridLayout_31"));
        label_23 = new QLabel(groupBox);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        gridLayout_31->addWidget(label_23, 0, 0, 1, 1);

        doubleSpinBox_18 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_18->setObjectName(QString::fromUtf8("doubleSpinBox_18"));
        doubleSpinBox_18->setDecimals(6);
        doubleSpinBox_18->setMinimum(0.000001000000000);
        doubleSpinBox_18->setMaximum(1000000000000000.000000000000000);
        doubleSpinBox_18->setValue(0.000001000000000);

        gridLayout_31->addWidget(doubleSpinBox_18, 0, 1, 1, 1);


        gridLayout_29->addLayout(gridLayout_31, 1, 0, 1, 1);

        gridLayout_32 = new QGridLayout();
        gridLayout_32->setSpacing(6);
        gridLayout_32->setObjectName(QString::fromUtf8("gridLayout_32"));
        label_24 = new QLabel(groupBox);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        gridLayout_32->addWidget(label_24, 0, 0, 1, 1);

        doubleSpinBox_19 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_19->setObjectName(QString::fromUtf8("doubleSpinBox_19"));
        doubleSpinBox_19->setDecimals(6);
        doubleSpinBox_19->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_19->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_19->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_32->addWidget(doubleSpinBox_19, 0, 1, 1, 1);


        gridLayout_29->addLayout(gridLayout_32, 2, 0, 1, 1);


        gridLayout_33->addLayout(gridLayout_29, 1, 7, 2, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        doubleSpinBox_2 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_2->setObjectName(QString::fromUtf8("doubleSpinBox_2"));
        doubleSpinBox_2->setDecimals(6);
        doubleSpinBox_2->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_2->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_2->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_2->addWidget(doubleSpinBox_2, 0, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        doubleSpinBox_3 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_3->setObjectName(QString::fromUtf8("doubleSpinBox_3"));
        doubleSpinBox_3->setDecimals(6);
        doubleSpinBox_3->setMinimum(0.000001000000000);
        doubleSpinBox_3->setMaximum(1000000000000000.000000000000000);
        doubleSpinBox_3->setValue(0.000001000000000);

        gridLayout->addWidget(doubleSpinBox_3, 0, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout, 1, 0, 1, 1);


        gridLayout_33->addLayout(gridLayout_3, 2, 0, 1, 1);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setSpacing(6);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout_8 = new QGridLayout();
        gridLayout_8->setSpacing(6);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_8->addWidget(label_5, 0, 0, 1, 1);

        doubleSpinBox_6 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_6->setObjectName(QString::fromUtf8("doubleSpinBox_6"));
        doubleSpinBox_6->setDecimals(6);
        doubleSpinBox_6->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_6->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_6->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_8->addWidget(doubleSpinBox_6, 0, 1, 1, 1);


        gridLayout_7->addLayout(gridLayout_8, 0, 0, 1, 1);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_9->addWidget(label_6, 0, 0, 1, 1);

        doubleSpinBox_7 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_7->setObjectName(QString::fromUtf8("doubleSpinBox_7"));
        doubleSpinBox_7->setDecimals(6);
        doubleSpinBox_7->setMinimum(0.000001000000000);
        doubleSpinBox_7->setMaximum(1000000000000000.000000000000000);
        doubleSpinBox_7->setValue(0.000001000000000);

        gridLayout_9->addWidget(doubleSpinBox_7, 0, 1, 1, 1);


        gridLayout_7->addLayout(gridLayout_9, 1, 0, 1, 1);

        gridLayout_19 = new QGridLayout();
        gridLayout_19->setSpacing(6);
        gridLayout_19->setObjectName(QString::fromUtf8("gridLayout_19"));
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        gridLayout_19->addWidget(label_13, 0, 0, 1, 1);

        doubleSpinBox_14 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_14->setObjectName(QString::fromUtf8("doubleSpinBox_14"));
        doubleSpinBox_14->setDecimals(6);
        doubleSpinBox_14->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_14->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_14->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_19->addWidget(doubleSpinBox_14, 0, 1, 1, 1);


        gridLayout_7->addLayout(gridLayout_19, 2, 0, 1, 1);


        gridLayout_33->addLayout(gridLayout_7, 2, 1, 1, 1);

        gridLayout_10 = new QGridLayout();
        gridLayout_10->setSpacing(6);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        gridLayout_11 = new QGridLayout();
        gridLayout_11->setSpacing(6);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_11->addWidget(label_7, 0, 0, 1, 1);

        doubleSpinBox_8 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_8->setObjectName(QString::fromUtf8("doubleSpinBox_8"));
        doubleSpinBox_8->setDecimals(6);
        doubleSpinBox_8->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_8->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_8->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_11->addWidget(doubleSpinBox_8, 0, 1, 1, 1);


        gridLayout_10->addLayout(gridLayout_11, 0, 0, 1, 1);

        gridLayout_12 = new QGridLayout();
        gridLayout_12->setSpacing(6);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_12->addWidget(label_8, 0, 0, 1, 1);

        doubleSpinBox_9 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_9->setObjectName(QString::fromUtf8("doubleSpinBox_9"));
        doubleSpinBox_9->setDecimals(6);
        doubleSpinBox_9->setMinimum(0.000001000000000);
        doubleSpinBox_9->setMaximum(1000000000000000.000000000000000);
        doubleSpinBox_9->setValue(0.000001000000000);

        gridLayout_12->addWidget(doubleSpinBox_9, 0, 1, 1, 1);


        gridLayout_10->addLayout(gridLayout_12, 1, 0, 1, 1);


        gridLayout_33->addLayout(gridLayout_10, 2, 2, 1, 1);

        gridLayout_13 = new QGridLayout();
        gridLayout_13->setSpacing(6);
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        gridLayout_14 = new QGridLayout();
        gridLayout_14->setSpacing(6);
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_14->addWidget(label_9, 0, 0, 1, 1);

        doubleSpinBox_10 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_10->setObjectName(QString::fromUtf8("doubleSpinBox_10"));
        doubleSpinBox_10->setDecimals(6);
        doubleSpinBox_10->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_10->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_10->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_14->addWidget(doubleSpinBox_10, 0, 1, 1, 1);


        gridLayout_13->addLayout(gridLayout_14, 0, 0, 1, 1);

        gridLayout_15 = new QGridLayout();
        gridLayout_15->setSpacing(6);
        gridLayout_15->setObjectName(QString::fromUtf8("gridLayout_15"));
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_15->addWidget(label_10, 0, 0, 1, 1);

        doubleSpinBox_11 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_11->setObjectName(QString::fromUtf8("doubleSpinBox_11"));
        doubleSpinBox_11->setDecimals(6);
        doubleSpinBox_11->setMinimum(0.000001000000000);
        doubleSpinBox_11->setMaximum(1000000000000000.000000000000000);
        doubleSpinBox_11->setValue(0.000001000000000);

        gridLayout_15->addWidget(doubleSpinBox_11, 0, 1, 1, 1);


        gridLayout_13->addLayout(gridLayout_15, 1, 0, 1, 1);


        gridLayout_33->addLayout(gridLayout_13, 2, 3, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_5->addWidget(label_3, 0, 0, 1, 1);

        doubleSpinBox_4 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_4->setObjectName(QString::fromUtf8("doubleSpinBox_4"));
        doubleSpinBox_4->setDecimals(6);
        doubleSpinBox_4->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_4->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_4->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_5->addWidget(doubleSpinBox_4, 0, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_5, 0, 0, 1, 1);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setSpacing(6);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout_6->addWidget(label_4, 0, 0, 1, 1);

        doubleSpinBox_5 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_5->setObjectName(QString::fromUtf8("doubleSpinBox_5"));
        doubleSpinBox_5->setDecimals(6);
        doubleSpinBox_5->setMinimum(0.000001000000000);
        doubleSpinBox_5->setMaximum(1000000000000000.000000000000000);
        doubleSpinBox_5->setValue(0.000001000000000);

        gridLayout_6->addWidget(doubleSpinBox_5, 0, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_6, 1, 0, 1, 1);


        gridLayout_33->addLayout(gridLayout_4, 2, 4, 1, 1);

        gridLayout_16 = new QGridLayout();
        gridLayout_16->setSpacing(6);
        gridLayout_16->setObjectName(QString::fromUtf8("gridLayout_16"));
        gridLayout_17 = new QGridLayout();
        gridLayout_17->setSpacing(6);
        gridLayout_17->setObjectName(QString::fromUtf8("gridLayout_17"));
        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_17->addWidget(label_11, 0, 0, 1, 1);

        doubleSpinBox_12 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_12->setObjectName(QString::fromUtf8("doubleSpinBox_12"));
        doubleSpinBox_12->setDecimals(6);
        doubleSpinBox_12->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_12->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_12->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_17->addWidget(doubleSpinBox_12, 0, 1, 1, 1);


        gridLayout_16->addLayout(gridLayout_17, 0, 0, 1, 1);

        gridLayout_18 = new QGridLayout();
        gridLayout_18->setSpacing(6);
        gridLayout_18->setObjectName(QString::fromUtf8("gridLayout_18"));
        label_12 = new QLabel(groupBox);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        gridLayout_18->addWidget(label_12, 0, 0, 1, 1);

        doubleSpinBox_13 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_13->setObjectName(QString::fromUtf8("doubleSpinBox_13"));
        doubleSpinBox_13->setDecimals(6);
        doubleSpinBox_13->setMinimum(0.000001000000000);
        doubleSpinBox_13->setMaximum(1000000000000000.000000000000000);
        doubleSpinBox_13->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);
        doubleSpinBox_13->setValue(0.000001000000000);

        gridLayout_18->addWidget(doubleSpinBox_13, 0, 1, 1, 1);


        gridLayout_16->addLayout(gridLayout_18, 1, 0, 1, 1);


        gridLayout_33->addLayout(gridLayout_16, 2, 5, 1, 1);

        gridLayout_20 = new QGridLayout();
        gridLayout_20->setSpacing(6);
        gridLayout_20->setObjectName(QString::fromUtf8("gridLayout_20"));
        gridLayout_27 = new QGridLayout();
        gridLayout_27->setSpacing(6);
        gridLayout_27->setObjectName(QString::fromUtf8("gridLayout_27"));
        label_20 = new QLabel(groupBox);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        gridLayout_27->addWidget(label_20, 0, 0, 1, 1);

        doubleSpinBox_15 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_15->setObjectName(QString::fromUtf8("doubleSpinBox_15"));
        doubleSpinBox_15->setDecimals(6);
        doubleSpinBox_15->setMinimum(-10000000000000000146306952306748730309700429878646550592786107871697963642511482159104.000000000000000);
        doubleSpinBox_15->setMaximum(100000000000000004384584304507619735463404765184.000000000000000);
        doubleSpinBox_15->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

        gridLayout_27->addWidget(doubleSpinBox_15, 0, 1, 1, 1);


        gridLayout_20->addLayout(gridLayout_27, 0, 0, 1, 1);

        gridLayout_28 = new QGridLayout();
        gridLayout_28->setSpacing(6);
        gridLayout_28->setObjectName(QString::fromUtf8("gridLayout_28"));
        label_21 = new QLabel(groupBox);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        gridLayout_28->addWidget(label_21, 0, 0, 1, 1);

        doubleSpinBox_16 = new QDoubleSpinBox(groupBox);
        doubleSpinBox_16->setObjectName(QString::fromUtf8("doubleSpinBox_16"));
        doubleSpinBox_16->setDecimals(6);
        doubleSpinBox_16->setMinimum(0.000001000000000);
        doubleSpinBox_16->setMaximum(1000000000000000.000000000000000);
        doubleSpinBox_16->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);
        doubleSpinBox_16->setValue(0.000001000000000);

        gridLayout_28->addWidget(doubleSpinBox_16, 0, 1, 1, 1);


        gridLayout_20->addLayout(gridLayout_28, 1, 0, 1, 1);


        gridLayout_33->addLayout(gridLayout_20, 2, 6, 1, 1);


        gridLayout_36->addWidget(groupBox, 0, 0, 1, 1);

        gridLayout_21 = new QGridLayout();
        gridLayout_21->setSpacing(6);
        gridLayout_21->setObjectName(QString::fromUtf8("gridLayout_21"));
        label_19 = new QLabel(layoutWidget);
        label_19->setObjectName(QString::fromUtf8("label_19"));

        gridLayout_21->addWidget(label_19, 0, 0, 1, 1);

        lineEdit = new QLineEdit(layoutWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout_21->addWidget(lineEdit, 0, 1, 1, 1);


        gridLayout_36->addLayout(gridLayout_21, 1, 0, 1, 1);

        gridLayout_22 = new QGridLayout();
        gridLayout_22->setSpacing(6);
        gridLayout_22->setObjectName(QString::fromUtf8("gridLayout_22"));
        label_15 = new QLabel(layoutWidget);
        label_15->setObjectName(QString::fromUtf8("label_15"));

        gridLayout_22->addWidget(label_15, 0, 0, 1, 1);

        lineEdit_2 = new QLineEdit(layoutWidget);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        gridLayout_22->addWidget(lineEdit_2, 0, 1, 1, 1);


        gridLayout_36->addLayout(gridLayout_22, 2, 0, 1, 1);

        gridLayout_23 = new QGridLayout();
        gridLayout_23->setSpacing(6);
        gridLayout_23->setObjectName(QString::fromUtf8("gridLayout_23"));
        lineEdit_6 = new QLineEdit(layoutWidget);
        lineEdit_6->setObjectName(QString::fromUtf8("lineEdit_6"));

        gridLayout_23->addWidget(lineEdit_6, 0, 1, 1, 1);

        label_16 = new QLabel(layoutWidget);
        label_16->setObjectName(QString::fromUtf8("label_16"));

        gridLayout_23->addWidget(label_16, 0, 0, 1, 1);


        gridLayout_36->addLayout(gridLayout_23, 3, 0, 1, 1);

        gridLayout_24 = new QGridLayout();
        gridLayout_24->setSpacing(6);
        gridLayout_24->setObjectName(QString::fromUtf8("gridLayout_24"));
        lineEdit_3 = new QLineEdit(layoutWidget);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));

        gridLayout_24->addWidget(lineEdit_3, 0, 1, 1, 1);

        label_17 = new QLabel(layoutWidget);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        gridLayout_24->addWidget(label_17, 0, 0, 1, 1);


        gridLayout_36->addLayout(gridLayout_24, 4, 0, 1, 1);

        gridLayout_25 = new QGridLayout();
        gridLayout_25->setSpacing(6);
        gridLayout_25->setObjectName(QString::fromUtf8("gridLayout_25"));
        label_14 = new QLabel(layoutWidget);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        gridLayout_25->addWidget(label_14, 0, 0, 1, 1);

        lineEdit_4 = new QLineEdit(layoutWidget);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));

        gridLayout_25->addWidget(lineEdit_4, 0, 1, 1, 1);


        gridLayout_36->addLayout(gridLayout_25, 5, 0, 1, 1);

        gridLayout_26 = new QGridLayout();
        gridLayout_26->setSpacing(6);
        gridLayout_26->setObjectName(QString::fromUtf8("gridLayout_26"));
        label_18 = new QLabel(layoutWidget);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        gridLayout_26->addWidget(label_18, 0, 0, 1, 1);

        lineEdit_5 = new QLineEdit(layoutWidget);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));

        gridLayout_26->addWidget(lineEdit_5, 0, 1, 1, 1);


        gridLayout_36->addLayout(gridLayout_26, 6, 0, 1, 1);

        gridLayout_34 = new QGridLayout();
        gridLayout_34->setSpacing(6);
        gridLayout_34->setObjectName(QString::fromUtf8("gridLayout_34"));
        label_25 = new QLabel(layoutWidget);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        gridLayout_34->addWidget(label_25, 0, 0, 1, 1);

        lineEdit_7 = new QLineEdit(layoutWidget);
        lineEdit_7->setObjectName(QString::fromUtf8("lineEdit_7"));

        gridLayout_34->addWidget(lineEdit_7, 0, 1, 1, 1);


        gridLayout_36->addLayout(gridLayout_34, 7, 0, 1, 1);

        gridLayout_35 = new QGridLayout();
        gridLayout_35->setSpacing(6);
        gridLayout_35->setObjectName(QString::fromUtf8("gridLayout_35"));
        label_26 = new QLabel(layoutWidget);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        gridLayout_35->addWidget(label_26, 0, 0, 1, 1);

        lineEdit_8 = new QLineEdit(layoutWidget);
        lineEdit_8->setObjectName(QString::fromUtf8("lineEdit_8"));

        gridLayout_35->addWidget(lineEdit_8, 0, 1, 1, 1);


        gridLayout_36->addLayout(gridLayout_35, 8, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1689, 26));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        pushButton_close->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\205\320\276\320\264", nullptr));
        pushButton_8->setText(QApplication::translate("MainWindow", "\320\235\320\260\321\207\320\260\321\202\321\214 \321\200\320\260\320\261\320\276\321\202\321\203", nullptr));
        textEdit->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:7.8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:600;\">\320\237\321\200\320\276\320\263\321\200\320\260\320\274\320\274\320\260 \320\264\320\273\321\217 \320\275\320\260\321\205\320\276\320\266\320\264\320\265\320\275\320\270\321\217 \321\201 \320\277\320\276\320\274\320\276\321\211\321\214\321\216 \321\200\321\217\320\264\320\276\320\262 \320\234\320\260\320\272\320\273\320\276\321\200\320\265\320\275\320\260 \320\267\320\275\320\260\321\207\320\265\320\275\320\270\320\271 \320\264\320\260\320\275\320\275\321\213\321\205 \321\204\321\203\320\275\320\272\321\206"
                        "\320\270\320\271:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">1)e^x</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">2)sinx</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">3)cosx</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">4)(1+x)^a</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">5)ln(1+x)</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-"
                        "indent:0px;\"><span style=\" font-size:10pt;\">6)arctgx</span></p></body></html>", nullptr));
        groupBox_2->setTitle(QString());
        groupBox->setTitle(QString());
        pushButton_arctg->setText(QApplication::translate("MainWindow", "arctgx", nullptr));
        pushButton_function->setText(QApplication::translate("MainWindow", "(1+x)^a", nullptr));
        pushButton_lnx->setText(QApplication::translate("MainWindow", "ln(1+x)", nullptr));
        pushButton_cosx->setText(QApplication::translate("MainWindow", "cosx", nullptr));
        pushButton_sinx->setText(QApplication::translate("MainWindow", "sinx", nullptr));
        pushButton_eX->setText(QApplication::translate("MainWindow", "e^x", nullptr));
        pushButton_functionOnlyX->setText(QApplication::translate("MainWindow", "1/(1-x)", nullptr));
        pushButton_EndGeometricRow->setText(QApplication::translate("MainWindow", "(1-x^(m+1))/(1-x_", nullptr));
        label_22->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">X</span></p></body></html>", nullptr));
        label_23->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">E</span></p></body></html>", nullptr));
        label_24->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:18pt; font-weight:600;\">M</span></p></body></html>", nullptr));
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">X</span></p></body></html>", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">E</span></p></body></html>", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">X</span></p></body></html>", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">E</span></p></body></html>", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:18pt; font-weight:600;\">a</span></p></body></html>", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">X</span></p></body></html>", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">E</span></p></body></html>", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">X</span></p></body></html>", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">E</span></p></body></html>", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">X</span></p></body></html>", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">E</span></p></body></html>", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">X</span></p></body></html>", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">E</span></p></body></html>", nullptr));
        label_20->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">X</span></p></body></html>", nullptr));
        label_21->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600;\">E</span></p></body></html>", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "e^x =", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "sinx =", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "cosx =", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "ln(1+x) =", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "(1+x)^a =", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "arctgx =", nullptr));
        label_25->setText(QApplication::translate("MainWindow", "(1-x^(m+1))/(1-x) =", nullptr));
        label_26->setText(QApplication::translate("MainWindow", "1/(1-x) =", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
