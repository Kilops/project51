QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_tests.cpp \
    tests.cpp \
    ../arctgx.cpp \
    ../cosx.cpp \
    ../edegree.cpp \
    ../functionx.cpp \
    ../helpfunction.cpp \
    ../ln.cpp \
    ../sinx.cpp \
    test_edegree.cpp \
    test_cosx.cpp \
    test_functionx.cpp \
    test_lnx.cpp \
    test_sinx.cpp \
    test_arctgx.cpp \
    test_functinononlyx.cpp \
    test_endgeometricrow.cpp \
    ../endgeometricrow.cpp \
    ../functiononlyx.cpp


HEADERS += \
    tests.h \
    ../function.h

